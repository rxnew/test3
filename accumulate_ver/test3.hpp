#include <iostream>
#include <vector>

namespace test3 {
template <class E, template <class...> class T = std::vector>
auto input(std::istream& os = std::cin) -> T<E> {
  T<E> v;
  E e;
  while(os >> e) {
    v.push_back(e);
  }
  return std::move(v);
}
}
