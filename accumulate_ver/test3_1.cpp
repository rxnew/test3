#include <numeric>

#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<int>();
  std::cout << std::accumulate(v.cbegin(), v.cend(), 0, std::max<int>) << std::endl;
  return 0;
}
