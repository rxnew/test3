#include <numeric>

#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<std::string>();
  auto f = [](const std::string& lhs, const std::string& rhs) {
    return lhs.length() < rhs.length() ? rhs : lhs;
  };
  std::cout << std::accumulate(v.cbegin(), v.cend(), std::string(), f) << std::endl;
  return 0;
}
