#include <numeric>

#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<std::string>();
  std::cout << std::accumulate(v.cbegin(), v.cend(), std::string(), std::max<std::string>) << std::endl;
  return 0;
}
