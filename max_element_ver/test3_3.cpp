#include <algorithm>

#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<std::string>();
  auto f = [](const std::string& lhs, const std::string& rhs) {
    return lhs.length() < rhs.length();
  };
  std::cout << *std::max_element(v.cbegin(), v.cend(), f) << std::endl;
  return 0;
}
