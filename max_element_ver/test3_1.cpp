#include <algorithm>

#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<int>();
  std::cout << *std::max_element(v.cbegin(), v.cend()) << std::endl;
  return 0;
}
