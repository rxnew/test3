#include <iostream>
#include <vector>
#include <cassert>

namespace test3 {
template <template <class...> class T, class E,
          class Function>
auto solve(const T<E>& v, Function f) -> E {
  auto it = v.cbegin();
  assert(it != v.cend());
  E res = *it;
  while(++it != v.cend()) {
    if(f(res, *it)) res = *it;
  }
  return res;
}

template <class E, template <class...> class T = std::vector>
auto input() -> T<E> {
  T<E> v;
  E e;
  while(std::cin >> e) {
    v.push_back(e);
  }
  return std::move(v);
}
}
