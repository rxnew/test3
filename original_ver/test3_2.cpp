#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<std::string>();
  auto f = std::less<std::string>();
  std::cout << test3::solve(v, f) << std::endl;
  return 0;
}
