#include "test3.hpp"

auto main() -> int {
  auto v = test3::input<int>();
  auto f = std::less<int>();
  std::cout << test3::solve(v, f) << std::endl;
  return 0;
}
